FROM python:3.11.1-alpine3.17

RUN mkdir app
#RUN python3 -m venv /opt/venv
WORKDIR /app

ENV PATH="${PATH}:/home/spock/.local/bin"
ENV PYTHONPATH=.

RUN apk update
RUN apk add musl-dev gcc 
RUN apk add net-snmp-dev
COPY requirements.txt .
RUN pip install --upgrade pip
RUN pip install -r requirements.txt

COPY clear_inactive_onts.py .
COPY dasanjanitor.conf .
COPY cfg.py .
COPY pon .

CMD ["python", "clear_inactive_onts.py", "-d", "-c", "/app/dasanjanitor.conf"]
