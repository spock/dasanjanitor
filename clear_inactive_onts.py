#!/usr/bin/env python3

from easysnmp import Session
import easysnmp
import sys
import argparse
import logging
import signal
from time import sleep
import json
import cfg


AUTHOR="v0.2 (c) Marcin Jurczuk <marcin@jurczuk.eu>"
SLEEP_DEFAULT=2
DAEMON_INTERVAL=45
ONT_STATUS=".1.3.6.1.4.1.6296.101.23.3.1.1.2"
sleGponOnuControlRequest=".1.3.6.1.4.1.6296.101.23.3.2.1.0"
sleGponOnuControlOltId=".1.3.6.1.4.1.6296.101.23.3.2.6.0"
sleGponOnuControlId=".1.3.6.1.4.1.6296.101.23.3.2.7.0"
sleGponOnuControlTimer=".1.3.6.1.4.1.6296.101.23.3.2.3.0"


def signal_handler(sig,frame):
    print("CTRL-C pressed. Exiting")
    sys.exit(0)

def is_inactive(snmpdata) -> bool:
    status = snmpdata.value
    try:
        value = int(status)
        if value == 1:
            return True
        else:
            return False
    except:
        return False

def remove_ont(olt_ip: str, olt_port: int,ont_id: int):
    snmp = Session(hostname=olt_ip,community="private", version=2)
    logging.debug("remove_ont(): Deleting ONT {}:{}".format(olt_port,ont_id))
    oid_values = [ (sleGponOnuControlRequest,2,'INTEGER'), (sleGponOnuControlOltId, olt_port, "INTEGER"), (sleGponOnuControlId, ont_id,"INTEGER"), (sleGponOnuControlTimer,0,"INTEGER")]
    try:
        ret=snmp.set_multiple(oid_values)
        #print("Return SNMPSET: {}".format(ret))
        return True
    except Exception as e:
        logging.warning("remove_ont() {}:{} exception: {}".format(olt_port,ont_id,e))
        return False


## this is so Golangish :)
def get_config(path: str) -> dict:
    try:
        with open(path) as cfg:
            config = json.load(cfg)
        return config,None
    except Exception as e:
        return None, e

def process_olt(olt_ip,opts):
    try:
        snmp = Session(hostname=olt_ip,community="public", version=2, use_numeric=True)
        onts_status = snmp.bulkwalk(ONT_STATUS)
        logging.info("[{}] getting data ..".format(olt_ip))
        inactive_onts = list(filter(is_inactive, onts_status))
        logging.info("[{}] {} out of {} is inactive".format(olt_ip,len(inactive_onts), len(onts_status)))
        for ont in inactive_onts:
            olt_port = int(ont.oid.split(".")[-1])
            ont_id = int(ont.oid_index)
            #print("OLT_PORT: {}, ONT_ID:{}".format(olt_port,ont_id))
            #index="{}.{}".format(ont_port,ont_id)
            #olt_port, ont_id = ont.oid_index.split('.')
            if not opts.list_only:
                if (remove_ont(olt_ip,olt_port,ont_id)):
                    logging.info("[{}] delete {}:{} OK".format(olt_ip,olt_port,ont_id)) 
                else:
                    logging.warning("[{}] delete {}:{} FAIL".format(olt_ip,olt_port,ont_id))
                sleep(opts.sleep)
            else:
                    print("PORT: {}, ONT: {}".format(olt_port,ont_id))
            
    except easysnmp.exceptions.EasySNMPTimeoutError as e:
            logging.warning("[{}] SNMP Error: {}".format(olt_ip,e))
    except Exception as e:
            logging.info("[{}] unexpected error: {}".format(olt_ip,e))
        
def run(args):
    parser = argparse.ArgumentParser(description='Remove Inactive ONTS from DASAN OLTS', epilog=AUTHOR)
    parser.add_argument('-r','--rocommunity', dest="rocommunity", default="public", help="RO OLT community (default public)")
    parser.add_argument('-w','--rwcommunity', dest="rwcommunity", default="private", help="RW OLT community (default private)")
    parser.add_argument('-c','--config', dest="config", default="/etc/dasanjanitor.conf", help="Config file required by daemon mode")
    parser.add_argument('-d','--daemon', dest="daemon", action="store_true", help="Enable daemon mode when using with systemd")
    parser.add_argument('-i','--interval', dest="interval", type=int,default=DAEMON_INTERVAL, help="On daemon mode how often run removal in minutes")
    parser.add_argument('-s','--sleep', dest="sleep", type=int, default=SLEEP_DEFAULT, help="Sleep in seconds between each ONT action. Some OLTS can't handle fast SNMP (default {}s)".format(SLEEP_DEFAULT))
    parser.add_argument('-l','--list', dest="list_only",action="store_true",help="List only - do not remove Inactive ONTS. Just show them.")
    parser.add_argument("ipaddress", help="OLT IP address",default="",nargs="?")
    opts = parser.parse_args()
    if not opts.daemon:
        olt_ip = opts.ipaddress
        process_olt(olt_ip,opts)
    else:
        try:
            config= cfg.get_config(opts.config)
        except Exception as e:
            print("{}".format(e))
            sys.exit(-1)

        logging.info("Staring daemon mode with removal every {} minutes".format(opts.interval))
        while True:
            for olt in config['olt']:
                try:
                    opts.rocommunity=olt['ro']
                    opts.rocommunity=olt['rw']
                    descr=olt.get('name',olt['ipaddress'])
                    logging.info("Calling process_olt() for {}".format(descr))
                    process_olt(olt['ipaddress'], opts)
                except Exception as e:
                    logging.warning("Error setting OLT params: {}".format(e))
                    continue
            logging.info("Sleeping for {} minutes".format(opts.interval))
            sleep(60*opts.interval)


            

         
        
    
        

if __name__ == "__main__":
    signal.signal(signal.SIGINT,signal_handler)
    logging.basicConfig(format="%(asctime)s %(message)s",datefmt="%Y-%m-%dT%H:%M:%S%z",level=logging.INFO)
    run(sys.argv)



