# dasanjanitor

Some tools to make life easier to users using DASAN OLT's.

## clear\_inactive\_onts.py

This script is removing Inactive ONTS that might be moved/removed and still they allocate OND ID  
Application supports two modes:
- daemon mode where all config data are loaded from config file. Best to run via systemd. Only -d option is really neeed.
- manual/cron mode where all config data are loaded from command options. You should provide -r -w flags  

**clear\_inactive\_onts.py -h** will give you help.


    usage: clear_inactive_onts.py [-h] [-r ROCOMMUNITY] [-w RWCOMMUNITY]
                                  [-c CONFIG] [-d] [-i INTERVAL] [-s SLEEP] [-l]
                                  [ipaddress]

    Remove Inactive ONTS from DASAN OLTS

    positional arguments:
      ipaddress             OLT IP address

    optional arguments:
      -h, --help            show this help message and exit
      -r ROCOMMUNITY, --rocommunity ROCOMMUNITY
                            RO OLT community (default public)
      -w RWCOMMUNITY, --rwcommunity RWCOMMUNITY
                            RW OLT community (default private)
      -c CONFIG, --config CONFIG
                            Config file required by daemon mode
      -d, --daemon          Enable daemon mode when using with systemd
      -i INTERVAL, --interval INTERVAL
                            On daemon mode how often run removal in minutes
      -s SLEEP, --sleep SLEEP
                            Sleep in seconds between each ONT action. Some OLTS
                            can't handle fast SNMP (default 2s)
      -l, --list            List only - do not remove Inactive ONTS. Just show
                            them.

    v0.2 (c) Marcin Jurczuk <marcin@jurczuk.eu>

Warning:
-d mode requires config file (default /etc/dasanjanitor.conf) with OLT SNMP credentials which we want to clean.



