from time import time



def influx_timestamp():
    ts=str(int(time()*1000))
    return ts + (19-len(ts))*'0'

class ONT(object):

    def __init__(self,serial,olt=None,port=None,port_id=None):
        self._serial = serial
        self._rx_level=-40
        self._olt = olt
        self._port = port
        self._port_id = port_id

    @property
    def serial(self):
        return self._serial

    @serial.setter
    def serial(self,serial):
        self._serial = serial

    @property
    def rxlevel(self):
        return self._rx_level

    @rxlevel.setter
    def rxlevel(self,level):
        self._rx_level = level
   
    @property
    def olt(self):
        return self._olt 
    @property
    def port(self):
        return self._port

    @port.setter
    def port(self,port):
        try:
            _port = int(port)
            if _port >= 0:
                self._port = _port
            else:
                raise Exception("Negative OLT port!")
        except:
                raise Exception("OLT port must be non-negative number")
    @property
    def id(self):
        return self._port_id 

    def __repr__(self):
        return f'{self.serial} {self.rxlevel} dBm ({self.olt}/{self.port}/{self.id})'
    def to_influx(self,measurement="GPONRX"):
        return f'{measurement},device_name={self.olt},olt_port={self.port},ont_port_id={self.id},ont_serial={self.serial} ont_rxlevel={self.rxlevel} {influx_timestamp()}' 

    
               
