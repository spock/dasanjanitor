
from pon.ont import ONT
from typing import List
from easysnmp import Session
import pon.oid as oid
import logging


ONTS = List[ONT]

class OLT:
    def __init__(self,host,rocommunity='public',rwcommunity='private',snmp_version=2):
        self._host = host
        self._rocommunity=rocommunity
        self._rwcommunity=rwcommunity
        self._snmp_version = snmp_version

    @property
    def host(self):
        return self._host

    @property
    def ip(self):
        return self._host
    @property
    def rocomm(self):
        return self._rocommunity
    
    @property
    def rwcomm(self):
        return self._rwcommunity
    
    @property
    def snmp_version(self):
        return self._snmp_version
    
    def __repr__(self):
        return self._host



# Create olts from config data
def create_olts(olts):
    result = []
    for elem in olts:
        #print(elem['name'])
        ipaddress = elem['ipaddress']
        ro = elem['ro']
        rw = elem['rw']
        #print(ipaddress)
        newolt = OLT(ipaddress, ro, rw, snmp_version=2)
        result.append(newolt)
    return result

def make_session(olt: OLT):
    device = Session(hostname=olt.ip,community=olt.rocomm,version=olt.snmp_version,use_numeric=True)
    return device


def get_name(olt: OLT) -> str:
    device = make_session(olt)
    try:
        val = device.get(oid.sysName)
        return val.value
    except:
        return ""
    
def get_onts(olt: OLT) -> ONTS:
    result = []
    ont_dict={}
    device = make_session(olt)
    device_name = get_name(olt)
    try:
        onts = device.bulkwalk(oid.sleGponOnuSerial)
        #print(onts)
        for elem in onts:
            ## we assume no mib resolv therefore we calculate values ourselfs
            #print(elem.oid_index)
            ont_port = elem.oid.split(".")[-1]
            ont_id = elem.oid_index
            index="{}.{}".format(ont_port,ont_id)
            ont_dev = ONT(elem.value,device_name,ont_port,ont_id) 
            ont_dict[index] = ont_dev
        rx_levels = device.bulkwalk(oid.sleGponOnuRxPower)
        for elem in rx_levels:
            rxlevel = int(elem.value) / 10.0
            ont_port = elem.oid.split(".")[-1]
            ont_id = elem.oid_index
            index="{}.{}".format(ont_port,ont_id)
            if ont_dict.get(index):
                ont_dict[index].rxlevel = rxlevel
        # make array 
        [result.append(x) for x in ont_dict.values()]
        return result
    except Exception as e:
            logging.warn("get_onts(): {}".format(e))
            print(f'{e}')



