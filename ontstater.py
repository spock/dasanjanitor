#!/usr/bin/env python3

import argparse
import sys
import logging
import cfg
import pon.olt as olt 
import pon.ont as ont
from influxdb import InfluxDBClient
from time import sleep

VERSION="0.0.1"
AUTHOR="Marcin Jurczuk <marcin@jurczuk.eu>"

def run(args):
    parser = argparse.ArgumentParser(description='Fetch ONT statistics from OLT', epilog=AUTHOR)
    parser.add_argument('-c','--config', dest="config", default="/etc/dasanjanitor.conf", help="Config file required by daemon mode")
    parser.add_argument('-d','--debug', dest="debug", action="store_true", help="Enable debug mode")
    parser.add_argument('-t','--test', dest="test", action="store_true", help="Just print values. No influx write")
    opts = parser.parse_args()
    conf = cfg.get_config(opts.config)
    #print(conf.get('olt'))
    olts = olt.create_olts(conf.get('olt'))
    influx_conf = conf['influxdb']
    try:
        ic = InfluxDBClient(influx_conf['host'], influx_conf['port'],influx_conf['username'], influx_conf['password'], influx_conf['database'])
    except Exception as e:
        logging.error("Unable to connectd to InfluxDB: {}".format(e))
        sys.exit(-1)
    ## main loop
    try:
        sleep_interval = conf['global']['pool_interval']
    except:
        logging.info("No pool_interval found in [global] config section. Defaulting to 300s")
        sleep_interval = 300
    while True:       
        for elem in olts:
            cpes = olt.get_onts(elem)
            insert_data = []
            for x in cpes:
                insert_data.append(x.to_influx())
            #print(insert_data)
            if not opts.test:
                try:
                    logging.info("Pushing data for OLT {} to database".format(elem.host))
                    ic.write_points(insert_data)
                except Exception as e:
                    logging.warning("Error sending data to InfluxDB: {}".format(e))
            else:
                logging.info("Showing data for OLT {}".format(elem.host))
                for x in insert_data:
                    print(x)
        logging.info("Sleeping for next {} seconds".format(sleep_interval))
        sleep(sleep_interval)    

if __name__ == "__main__":
    logging.basicConfig(format="%(asctime)s %(message)s",datefmt="%Y-%m-%dT%H:%M:%S%z",level=logging.INFO)
    logging.info("Starting onstater v{}".format(VERSION))
    run(sys.argv)