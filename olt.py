
from ont import ONT
from typing import List
from easysnmp import Session
import oid



ONTS = List[ONT]

class OLT:
    def __init__(self,host,rocommunity='public',rwcommunity='private',snmp_version=2):
        self._host = host
        self._rocommunity=rocommunity
        self._rwcommunity=rwcommunity
        self._snmp_version = snmp_version

    @property
    def host(self):
        return self._host

    @property
    def rocomm(self):
        return self._rocommunity
    
    @property
    def rwcomm(self):
        return self._rwcommunity
    
    @property
    def snmp_version(self):
        return self._snmp_version




def make_session(olt: OLT):
    device = Session(hostname=olt.host,community=olt.rocomm,version=olt.snmp_version)
    return device


def get_name(olt: OLT) -> str:
    device = make_session(olt)
    try:
        val = device.get(oid.sysName)
        return val.value
    except:
        return ""
    
def get_onts(olt: OLT) -> ONTS:
    result = []
    ont_dict={}
    device = make_session(olt)
    device_name = get_name(olt)
    try:
        onts = device.bulkwalk(oid.sleGponOnuSerial)
        for elem in onts:
            ont_port,ont_id = elem.oid_index.split('.')
            ont_dev = ONT(elem.value,device_name,ont_port,ont_id) 
            ont_dict[elem.oid_index] = ont_dev
        rx_levels = device.bulkwalk(oid.sleGponOnuRxPower)
        for elem in rx_levels:
            rxlevel = int(elem.value) / 10.0
            if ont_dict.get(elem.oid_index):
                ont_dict[elem.oid_index].rxlevel = rxlevel
        # make array 
        [result.append(x) for x in ont_dict.values()]
        return result
    except Exception as e:
            print(f'{e}')



